﻿using UnityEngine;
using TMPro;
using DG.Tweening;
using System;

public class TrackTime : MonoBehaviour
{
    public TextMeshProUGUI TimerText;
    //public Transform Omo;
    //public Vector2 NextPosition;
    //public float AnimTime = 2f;
    //public float Delay = 2f;
    //public Ease Ease = Ease.Linear;

    public AudioSource audioSoundForRemainTime;

    private float _timer;
    //private RectTransform _rect;
    //private Transform x;

    void Start()
    {
        _timer = Level.level.GetTime();
    }
    void Awake()
    {
    //    _rect = GetComponent<RectTransform>();

        // _rect.DOAnchorPos(NextPosition, AnimTime).SetDelay(Delay).SetEase(Ease);

        // Omo.DOMove(Omo.position + new Vector3(6, 0, 0), 1f).SetDelay(2f).SetEase(Ease);
    }

    // Update is called once per frame
    void Update()
    {
        double timerValue = Math.Round(_timer, 1);
        if (timerValue != 0)
        {
            _timer -= Time.deltaTime;
        }
        //print(timerValue);
        TimerText.text = _timer.ToString("00.0");
        Level.score = TimerText.text;
        if (timerValue == 0)
        {
            Level.level.isFinished = true;
            SceneController controller = new SceneController();
            controller.LoadLevelSelectionScene();
        }

        if (timerValue == 22)
        {
            audioSoundForRemainTime.Play();
        }
    }

}
