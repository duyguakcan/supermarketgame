﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ShelfObjectManager
{

    List<ShelfObject> objectList = new List<ShelfObject>();
    public List<ShelfObject> GetList()
    {
        return objectList;
    }
    public List<ShelfObject> GetListByLevel(string levelName)
    {
        return objectList.Where(h => h.level == levelName).ToList();
    }


    public void CreateObject()
    {
        //FileManager fm = new FileManager(FilePathList.Resources);
        //List<string> resources = fm.Read();
        //foreach (var lineItem in resources)
        //{
        //    var items = lineItem.Split(new char[] { ',' });
        //    ShelfObject obj = new ShelfObject(items[0], items[1], items[2], items[3], float.Parse(items[4]), float.Parse(items[5]), float.Parse(items[6]), items[7]);
        //    Debug.LogWarning(items[0] + items[1] + items[2] + items[3] + float.Parse(items[4]) + float.Parse(items[5]) + float.Parse(items[6]) + items[7]);
        //    objectList.Add(obj);
        //}

        objectList = Level.level.shelfObjects;
    }

}


