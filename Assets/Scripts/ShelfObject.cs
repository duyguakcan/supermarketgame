﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShelfObject
{
    //private string p1;
    //private bool p2;
    //private string p3;
    //private string p4;
    //private float p5;
    //private float p6;
    //private float p7;
    //private Quaternion quaternion;
    //private string p8;
    
    public string level { set; get; }
    public string path { set; get; }
    public string name { set; get; }
    public float px { set; get; }
    public float py { set; get; }
    public float pz { set; get; }
    public string shelfName { set; get; }
    public bool visibleInList { set; get; }
    public float sx { set; get; }
    public float sy { set; get; }
    public float sz { set; get; }
    public Quaternion rotation { set; get; }
    public string activateShelfName { set; get; }
    public ShelfObject(string level, bool visibleInList, string path, string name,
    float px, float py, float pz, Quaternion rotation, string shelfName,
         float sx, float sy, float sz,string activateShelfName)
    {
        this.level = level;
        this.visibleInList = visibleInList;
        this.path = path;
        this.name = name;
        this.px = px;
        this.py = py;
        this.pz = pz;
        this.shelfName = shelfName;
        this.rotation = rotation;
        this.sx = sx;
        this.sy = sy;
        this.sz = sz;
        this.activateShelfName = activateShelfName;
    }

}
