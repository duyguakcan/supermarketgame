﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    public GameObject loadingScreen;
    public TextMeshProUGUI notificationText;
    public Slider slider;

    public GameObject gameOverScreen;
    public TextMeshProUGUI gameOverText;
    public void LoadLevel(string level)
    {
        notificationText.text = "";
        if (!string.IsNullOrEmpty(Level.userName))
        {
            if (level == "Level1")
                Level.level = new Level1();
            if (level == "Level2")
                Level.level = new Level2();
            if (level == "Level3")
                Level.level = new Level3();

            StartCoroutine(LoadAsynchronously());
        }
        else
        {
            notificationText.text = "Kullanıcı seçiniz.";
        }
    }
    IEnumerator LoadAsynchronously()
    {
        SceneController controller = new SceneController();
        AsyncOperation operation = controller.LoadSupermarketScene();
        
        loadingScreen.SetActive(true);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            yield return null;
        }
    }
    void Update()
    {
        if (Level.level != null && Level.level.isFinished == true)
        {
            gameOverScreen.SetActive(true);
            if (Level.score == "00.0")
                gameOverText.text = "Yeniden dene " + Level.userName + ". Skorun: " + Level.score;
            else
                gameOverText.text = "Tebrikler " + Level.userName + ". Skorun: " + Level.score;
        }
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    

}
