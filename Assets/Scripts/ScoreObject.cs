﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class ScoreObject
{
    public string userName { set; get; }
    public int levelNumber { set; get; }
    public double score { set; get; }
}

