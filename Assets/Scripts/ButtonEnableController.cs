﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEnableController : MonoBehaviour {

    public Button level1Button;
    public Button level2Button;
    public Button level3Button;

    void Start()
    {

        ControlLevelButtonEnabled();
    }
    public void Update()
    {
        if (Level.level != null && Level.level.isFinished == true)
            ControlLevelWhenLevelIsFinished();
    }
    public void SetButtonEnableWhenUserSelected()
    {
        ControlLevelButtonEnabled();
    }

    private void ControlLevelWhenLevelIsFinished()
    {
        if (Level.level.GetLevelName() == "L1" && !Level.score.Contains("00.0"))
            level2Button.enabled = true;
        else if (Level.level.GetLevelName() == "L2" && !Level.score.Contains("00.0"))
            level3Button.enabled = true;
    }
    private void ControlLevelButtonEnabled()
    {
        level1Button.enabled = false;
        level2Button.enabled = false;
        level3Button.enabled = false;

        if (!string.IsNullOrEmpty(Level.userName))
            level1Button.enabled = true;

        foreach (var item in Level.oldScoreList)
        {
            if (item.levelNumber == 1 && item.score > 0)
                level2Button.enabled = true;
            else if (item.levelNumber == 2 && item.score > 0)
                level3Button.enabled = true;
        }

    }





}
