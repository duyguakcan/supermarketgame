﻿using System.IO;
using System.Text;
using System.Collections.Generic;
using UnityEngine;


public class FileManager
{
    string path = "";
    public FileManager(string path)
    {
        this.path = Application.dataPath + path;
    }
    public List<string> Read()
    {
        List<string> items = new List<string>();
        using (StreamReader sr = new StreamReader(path, Encoding.UTF8, true))
        {
            int i = 0;
            while (sr.Peek() >= 0)
            {
                string text = sr.ReadLine();
                if (i > 0)
                {
                    items.Add(text);
                }
                i++;
            }
        }
        return items;
    }
    public string ReadByName(string name)
    {
        using (StreamReader sr = new StreamReader(path, Encoding.UTF8, true))
        {
            int i = 0;
            while (sr.Peek() >= 0)
            {
                string text = sr.ReadLine();
                if (i > 0)
                {
                    var itemArray = text.Split(new char[] {',' });
                    if (itemArray[0] == name)
                        return text;
                }
                i++;
            }
        }
        return null;
    }

    public FileObject ReadWithLine(string name)
    {
        FileObject obj = null;
        using (StreamReader sr = new StreamReader(path, Encoding.UTF8, true))
        {
            int i = 0;
            while (sr.Peek() >= 0)
            {
                string text = sr.ReadLine();
                if (i > 0)
                {
                    var itemArray = text.Split(new char[] {',' });
                    if (itemArray[0] == name)
                    {
                        obj = new FileObject();
                        obj.lineNumber = i;
                        obj.text = text;

                    }
                }
                i++;
            }
        }
        return obj;
    }

    public void Write(string text)
    {
        using (StreamWriter writer = new StreamWriter(path, true))
        {
            writer.Write("\n"+ text);
        }
    }
    public void WriteWithLine(string text,int lineNumber)
    {
        var list = this.Read();
        list[lineNumber-1] = text;
        WriteAllLines(list);
    }

    public void WriteAllLines(List<string> textList)
    {
        using (StreamWriter writer = new StreamWriter(path))
        {
            writer.Write("UserName,L1,L2,L3");
            foreach (var item in textList)
            {
                writer.Write("\n" + item);
            }
        }
    }


}

