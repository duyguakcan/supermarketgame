﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Shoppable
{

    public GameObject go;
    public bool found;

    public Shoppable(GameObject go, bool found)
    {
        this.go = go;
        this.found = found;
    }
}


