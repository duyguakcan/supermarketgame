﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class Level2 : BaseLevel
{

    public Level2()
        : base(110)
    {
        //clicklenebilir objeler - sek
        this.shelfObjects.Add(new ShelfObject("L2", true, "FromBlender/civciv_sut", "Civciv Süt",
            1.672f, 1.6772f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f,""));

        this.shelfObjects.Add(new ShelfObject("L2", true, "FromBlender/civciv_sut", "Civciv Süt",
            1.928f, 1.6772f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", true, "FromBlender/civciv_sut", "Civciv Süt",
            2.204f, 1.6772f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", true, "FromBlender/civciv_sut", "Civciv Süt",
            2.467f, 1.6772f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", true, "FromBlender/civciv_sut", "Civciv Süt",
            2.776f, 1.6772f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", true, "FromBlender/civciv_sut", "Civciv Süt",
            3.077f, 1.6772f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", true, "FromBlender/civciv_sut", "Civciv Süt",
            3.351f, 1.6772f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        //static objeler - pınar 
        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/sincap_sut", "Sincap Süt",
            1.672f, 1.266f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/sincap_sut", "Sincap Süt",
            1.928f, 1.266f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/sincap_sut", "Sincap Süt",
            2.204f, 1.266f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/sincap_sut", "Sincap Süt",
            2.467f, 1.266f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/sincap_sut", "Sincap Süt",
            2.776f, 1.266f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/sincap_sut", "Sincap Süt",
            3.077f, 1.266f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/sincap_sut", "Sincap Süt",
            3.351f, 1.266f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));


        //static objeler - burada başka bir marka olacak 
        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/pelikan_sut", "Pelikan Süt",
            1.672f, 2.075f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/pelikan_sut", "Pelikan Süt",
            1.928f, 2.075f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/pelikan_sut", "Pelikan Süt",
            2.204f, 2.075f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/pelikan_sut", "Pelikan Süt",
            2.467f, 2.075f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/pelikan_sut", "Pelikan Süt",
            2.776f, 2.075f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/pelikan_sut", "Pelikan Süt",
            3.077f, 2.075f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/pelikan_sut", "Pelikan Süt",
            3.351f, 2.075f, -22.1f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position2", 6f, 8f, 18f, ""));

        //*********************************************************//

        //ikinci grup marka ürünler buraya gelecek. gofret çeşitleri olacak.
        ///
        this.shelfObjects.Add(new ShelfObject("L2", true, "FromBlender/zebra_gofret", "Zebra Gofret",
            -11.197f, 1.741f, -39.451f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position3", 8f, 20f, 15f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", true, "FromBlender/zebra_gofret", "Zebra Gofret",
            -11.197f, 1.741f, -38.982f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position3", 8f, 20f, 15f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", true, "FromBlender/zebra_gofret", "Zebra Gofret",
            -11.197f, 1.741f, -38.507f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position3", 8f, 20f, 15f, ""));


        //1.301

        //0.847
        ///ikinci grup marka ürünler buraya gelecek. gofret çeşitleri olacak.
        ///
        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/tavuk_gofret", "Tavuk Gofret",
            -11.197f, 1.297f, -39.451f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position3", 8f, 20f, 15f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/tavuk_gofret", "Tavuk Gofret",
            -11.197f, 1.291f, -38.982f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position3", 8f, 20f, 15f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/tavuk_gofret", "Tavuk Gofret",
            -11.197f, 1.292f, -38.507f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position3", 8f, 20f, 15f, ""));

        /////////////////////

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/kus_gofret", "Kuş Gofret",
            -11.197f, 0.841f, -39.451f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position3", 8f, 20f, 15f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/kus_gofret", "Kuş Gofret",
            -11.197f, 0.841f, -38.982f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position3", 8f, 20f, 15f, ""));

        this.shelfObjects.Add(new ShelfObject("L2", false, "FromBlender/kus_gofret", "Kuş Gofret",
            -11.197f, 0.841f, -38.507f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position3", 8f, 20f, 15f, ""));


    }

    public override float GetTime()
    {
        return time;
    }

    public override string GetLevelName()
    {
        return "L2";
    }





}

