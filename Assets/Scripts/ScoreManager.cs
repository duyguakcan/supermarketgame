﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public TextMeshProUGUI userScoreListText;
    public TextMeshProUGUI notificationText;
    public InputField userName;
    public void SaveNewUser()
    {
        notificationText.text = "";
        FileManager fm = new FileManager(FilePathList.UserScores);
        string user = userName.text.ToLower();
        if (string.IsNullOrEmpty(fm.ReadByName(user)))
        {

            fm.Write(user + "," + "," + ",");
            //var obj = fm.ReadWithLine(user);
            //Level.fileObject = obj;
            Level.userName = user;
            Level.oldScoreList = GetScoreListByUserName();
            notificationText.text = "Yeni oyuncu eklendi.";
        }
        else
        {
            notificationText.text = "Bu oyuncu daha önce eklenmiştir. Devam edebilir ya da yeni oyuncu ekleyebilirsiniz.";
        }


    }
    public void ContinueSelectedUser()
    {
        notificationText.text = "";
        FileManager fm = new FileManager(FilePathList.UserScores);
        string user = userName.text.ToLower();
        if (!string.IsNullOrEmpty(fm.ReadByName(user)))
        {
            //var obj = fm.ReadWithLine(user);
            //Level.fileObject = obj;
            Level.userName = user;
            Level.oldScoreList = GetScoreListByUserName();
            notificationText.text = "Oyuncu seçildi. Seviyeyi seçerek devam ediniz.";

        }
        else
        {
            notificationText.text = "Bu oyuncu sistemde bulunmamaktadır. Yeni oyuncu ekleyeyiniz.";
        }
    }

    private void SaveUserScore()
    {
        FileManager fm = new FileManager(FilePathList.UserScores);
        var obj = fm.ReadWithLine(Level.userName);
        var fileTextArray = obj.text.Split(new char[] { ',' });

        if (Level.level.GetLevelName() == "L1")
            fileTextArray[1] = Level.score;
        if (Level.level.GetLevelName() == "L2")
            fileTextArray[2] = Level.score;
        if (Level.level.GetLevelName() == "L3")
            fileTextArray[3] = Level.score;
        fm.WriteWithLine(string.Join(",", fileTextArray), obj.lineNumber);
        Level.oldScoreList = GetScoreListByUserName();
    }

    void Start()
    {
        userScoreListText.text = CreateScoreList();
        if (!string.IsNullOrEmpty(Level.userName))
            userName.text = Level.userName;
    }
    void Update()
    {
        bool isSaved = false;
        if (Level.level != null)
        {
            if (Level.level.isFinished == true)
            {
                if (!isSaved)
                {
                    SaveUserScore();
                    userScoreListText.text = CreateScoreList();
                    isSaved = true;
                }
            }
        }
    }

    private string CreateScoreList()
    {
        string resultText = "<u><size=20><b><color=#B80F0FFF>Başarı Sırası</color></b></size></u>\n";
        List<ScoreObject>  objectList = GetScoreList();
        objectList = objectList.OrderByDescending(h => h.levelNumber).ThenByDescending(h => h.score).ToList();
        int number = 1;
        foreach (var item in objectList)
        {
            //Debug.LogWarning(item.levelNumber + " " + item.score);
            resultText += number.ToString()+ ". S" + item.levelNumber + "." + item.userName + "- Score:" + item.score.ToString() + "\n ";
            number = number + 1;
        }
        return resultText;
    }
    private List<ScoreObject> GetScoreList()
    {
        FileManager fm = new FileManager(FilePathList.UserScores);
        List<string> list = fm.Read();
        List<ScoreObject> objectList = new List<ScoreObject>();
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] != null)
            {
                var listItem = list[i].Split(new char[] { ',' });
                for (int j = 1; j < listItem.Length; j++)
                {
                    if (!string.IsNullOrEmpty(listItem[j]))
                    {
                        ScoreObject obj = new ScoreObject();
                        obj.userName = listItem[0];
                        obj.levelNumber = j;
                        if (!string.IsNullOrEmpty(listItem[j]))
                            obj.score = Convert.ToDouble(listItem[j]);
                        objectList.Add(obj);
                    }
                }
            }
        }
        return objectList;
    }

    private List<ScoreObject> GetScoreListByUserName()
    {
        List<ScoreObject> objectList = GetScoreList();
        return objectList.Where(h => h.userName == Level.userName).ToList();
    }



}
