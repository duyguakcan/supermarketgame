﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class BaseLevel
{
    public bool isFinished = false;
    public List<ShelfObject> shelfObjects = new List<ShelfObject>();
    public float time = 60;

    public BaseLevel(float time=60) {
        this.time = time;
    }

    public virtual string GetLevelName()
    {
        return "L2";
    }
    public virtual float GetTime()
    {
        return 0;
    }
}

