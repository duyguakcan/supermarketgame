﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class Level1 : BaseLevel
{
    public Level1()
        : base(120)
    {
        //clicklenebilir objeler - bingo
        this.shelfObjects.Add(new ShelfObject("L1", true, "FromBlender/aslan_deterjan", "Aslan Deterjan",
            3.224f, 1.2761f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f,""));

        this.shelfObjects.Add(new ShelfObject("L1", true, "FromBlender/aslan_deterjan", "Aslan Deterjan",
            2.774f, 1.2761f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L1", true, "FromBlender/aslan_deterjan", "Aslan Deterjan",
            2.318f, 1.2761f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L1", true, "FromBlender/aslan_deterjan", "Aslan Deterjan",
            1.84f, 1.2761f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f, ""));


        //static objeler - omo
        this.shelfObjects.Add(new ShelfObject("L1", false, "FromBlender/penguen_deterjan", "Penguen Deterjan",
            3.224f, 1.68f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L1", false, "FromBlender/penguen_deterjan", "Penguen Deterjan",
            2.774f, 1.68f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L1", false, "FromBlender/penguen_deterjan", "Penguen Deterjan",
            2.318f, 1.68f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L1", false, "FromBlender/penguen_deterjan", "Penguen Deterjan",
            1.84f, 1.68f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f, ""));

         //- boron
        this.shelfObjects.Add(new ShelfObject("L1", false, "FromBlender/keci_deterjan", "Keçi Deterjan",
            3.224f, 2.074f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L1", false, "FromBlender/keci_deterjan", "Keçi Deterjan",
            2.774f, 2.074f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L1", false, "FromBlender/keci_deterjan", "Keçi Deterjan",
            2.318f, 2.074f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L1", false, "FromBlender/keci_deterjan", "Keçi Deterjan",
            1.84f, 2.074f, -31.3f, Quaternion.Euler(new Vector3(-90, 90, 0)),
            "Position1", 6f, 14f, 20f, ""));



        

        //this.shelfObjects.Add(new ShelfObject("L1", true, "FromBlender/omo", "Omo Deterjan", 2.0f, 1.329f, -40.695f, Quaternion.Euler(new Vector3(90, 0, 90)), "Position1"));
        //this.shelfObjects.Add(new ShelfObject("L1", true, "FromBlender/pinar", "Pınar sut", 2.2f, 1.329f, -40.695f, Quaternion.Euler(new Vector3(90, 90, 0)), "Position1"));
        //this.shelfObjects.Add(new ShelfObject("L1", true, "FromBlender/sek", "Sek sut", 2.4f, 1.329f, -40.695f, Quaternion.Euler(new Vector3(0, 0, 90)), "Position1"));
    }
    public override float GetTime()
    {
        return time;
    }

    public override string GetLevelName()
    {
        return "L1";
    }




}

