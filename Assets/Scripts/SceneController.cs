﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneController : MonoBehaviour {

    public AsyncOperation LoadSupermarketScene()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("Supermarket_01");
        return operation;
    }
    public void LoadLevelSelectionScene()
    {
        SceneManager.LoadSceneAsync("Scene1");
    }
	
}
