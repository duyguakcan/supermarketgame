﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class Level3 : BaseLevel
{

    public Level3()
        : base(100)
    {
        //clicklenebilir objeler - sek
        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/geyik_cay", "Geyik Çay",
            1.846f, 1.283f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/geyik_cay", "Geyik Çay",
            2.1f, 1.283f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/geyik_cay", "Geyik Çay",
           2.382f, 1.283f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/geyik_cay", "Geyik Çay",
            2.685f, 1.283f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/geyik_cay", "Geyik Çay",
            3.038f, 1.283f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/geyik_cay", "Geyik Çay",
            3.325f, 1.283f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        //clicklenebilir objeler - sek
        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/tilki_cay", "Tilki Çay",
            1.846f, 1.686f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/tilki_cay", "Tilki Çay",
            2.1f, 1.686f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/tilki_cay", "Tilki Çay",
           2.382f, 1.686f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/tilki_cay", "Tilki Çay",
            2.685f, 1.686f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/tilki_cay", "Tilki Çay",
            3.038f, 1.686f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/tilki_cay", "Tilki Çay",
            3.325f, 1.686f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        //clicklenebilir objeler - sek
        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/baykus_cay", "Baykuş Çay",
            1.846f, 2.086f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/baykus_cay", "Baykuş Çay",
            2.1f, 2.086f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/baykus_cay", "Baykuş Çay",
           2.382f, 2.086f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/baykus_cay", "Baykuş Çay",
            2.685f, 2.086f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/baykus_cay", "Baykuş Çay",
            3.038f, 2.086f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/baykus_cay", "Baykuş Çay",
            3.325f, 2.086f, -40.35f, Quaternion.Euler(new Vector3(-90, -90, 0)),
            "Position4", 9f, 8f, 18f, ""));


        //******************************ikinci ürün******************peynir
        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/horoz_peynir", "Horoz Peynir",
            -7.806f, 1.729f, -20.009f, Quaternion.Euler(new Vector3(-90, 0, 0)),
            "Position5", 20f, 25f, 14f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/horoz_peynir", "Horoz Peynir",
            -7.806f, 1.729f, -20.572f, Quaternion.Euler(new Vector3(-90, 0, 0)),
            "Position5", 20f, 25f, 14f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/horoz_peynir", "Horoz Peynir",
            -7.806f, 1.729f, -21.156f, Quaternion.Euler(new Vector3(-90, 0, 0)),
            "Position5", 20f, 25f, 14f, ""));


        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/fil_peynir", "Fil Peynir",
            -7.806f, 2.188f, -20.009f, Quaternion.Euler(new Vector3(-90, 0, 0)),
            "Position5", 20f, 25f, 14f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/fil_peynir", "Fil Peynir",
            -7.806f, 2.188f, -20.572f, Quaternion.Euler(new Vector3(-90, 0, 0)),
            "Position5", 20f, 25f, 14f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/fil_peynir", "Fil Peynir",
            -7.806f, 2.188f, -21.156f, Quaternion.Euler(new Vector3(-90, 0, 0)),
            "Position5", 20f, 25f, 14f, ""));


        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/zurafa_peynir", "Zürafa Peynir",
            -7.806f, 1.286f, -20.009f, Quaternion.Euler(new Vector3(-90, 0, 0)),
            "Position5", 20f, 25f, 14f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/zurafa_peynir", "Zürafa Peynir",
            -7.806f, 1.286f, -20.572f, Quaternion.Euler(new Vector3(-90, 0, 0)),
            "Position5", 20f, 25f, 14f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/zurafa_peynir", "Zürafa Peynir",
            -7.806f, 1.286f, -21.156f, Quaternion.Euler(new Vector3(-90, 0, 0)),
            "Position5", 20f, 25f, 14f, ""));


        //******************************üçüncü ürün******************çikolata 

        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/panda_cikolata", "Panda Çikolata",
            -6.477f, 0.884f, -33.369f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position6", 15f, 24f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/panda_cikolata", "Panda Çikolata",
            -6.477f, 0.884f, -32.835f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position6", 15f, 24f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", true, "FromBlender/panda_cikolata", "Panda Çikolata",
            -6.477f, 0.884f, -32.255f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position6", 15f, 24f, 20f, ""));


        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/kelebek_cikolata", "Kelebek Çikolata",
            -6.477f, 1.349f, -33.369f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position6", 15f, 24f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/kelebek_cikolata", "Kelebek Çikolata",
            -6.477f, 1.347f, -32.835f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position6", 15f, 24f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/kelebek_cikolata", "Kelebek Çikolata",
            -6.477f, 1.348f, -32.255f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position6", 15f, 24f, 20f, ""));



        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/tavsan_cikolata", "Tavşan Çikolata",
            -6.477f, 1.794f, -33.369f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position6", 15f, 24f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/tavsan_cikolata", "Tavşan Çikolata",
            -6.477f, 1.794f, -32.835f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position6", 15f, 24f, 20f, ""));

        this.shelfObjects.Add(new ShelfObject("L3", false, "FromBlender/tavsan_cikolata", "Tavşan Çikolata",
            -6.477f, 1.794f, -32.255f, Quaternion.Euler(new Vector3(-90, 180, 0)),
            "Position6", 15f, 24f, 20f, ""));

        

    }

    public override float GetTime()
    {
        return time;
    }

    public override string GetLevelName()
    {
        return "L3";
    }






}

