﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ObjectClicker : MonoBehaviour {

    public ShoppingList shoppingList;
    public TextMeshProUGUI _shoppingListText;
    public AudioSource tickAudioSource;

	// Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Camera.allCameras[0].tag = "MainCamera";
            //RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            //if (Physics.Raycast(ray, out hit, 100.0f))
            //{
            //    if (hit.transform != null)
            //    {
            //        CheckItem(hit.transform.gameObject);
            //    }
            //}

            RaycastHit[] hits;
            hits = Physics.RaycastAll(ray, 100.0f);

            for (int i = 0; i < hits.Length; i++)
            {
                RaycastHit hit = hits[i];
                if (hit.transform != null)
                {
                    var result = CheckItem(hit.transform.gameObject);
                    if (result)
                    {
                        CheckTickSound();
                        GameIsFinisihed();
                        break;
                    }
                }
            }
        }
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }




    }

    private void CheckTickSound()
    {
        // If the left mouse button is pressed down...
        if (Input.GetMouseButtonDown(0) == true)
        {
            tickAudioSource.Play();
        }
        // If the left mouse button is released...
        if (Input.GetMouseButtonUp(0) == true)
        {
            tickAudioSource.Stop();
        }
    }

    void Start()
    {
        shoppingList = GetComponent<ShoppingList>();
        shoppingList.Initialize();
        //_shoppingListText = GameObject.FindGameObjectWithTag("ShoppingListText").GetComponent<Text>();
        _shoppingListText.text = shoppingList.GetListText();
    }

    private bool CheckItem(GameObject go)
    {
        var result = shoppingList.CheckItem(go);

        //_shoppingListText = GameObject.FindGameObjectWithTag("ShoppingListText").GetComponent<Text>();
        _shoppingListText.text = shoppingList.GetListText();

        return result;
    }

    private void GameIsFinisihed()
    {
        if (shoppingList.IsAllItemsFound())
        {
            Level.level.isFinished = true;

            SceneController controller = new SceneController();
            controller.LoadLevelSelectionScene();
        }
    }

}
