﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstSceneAudioManager : MonoBehaviour {
    public AudioSource firstSceneAudio;


	// Use this for initialization
    void Start()
    {
        System.Random random = new System.Random();
        int rInt = random.Next(1, 5);
        string fileName = rInt.ToString();

        firstSceneAudio.clip = Resources.Load<AudioClip>("Sounds/"+ fileName);
        firstSceneAudio.Play();

    }
	
}
