﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class ShoppingList : MonoBehaviour
{
    public List<Shoppable> listItems = new List<Shoppable>();
    public List<ShelfObject> shelfObjectList = new List<ShelfObject>();

    BoxCollider boxCollider;
    GameObject shelfObject;
    GameObject activateShelfObject;

    public void Initialize()
    {
        ShelfObjectManager mn = new ShelfObjectManager();
        mn.CreateObject();
        shelfObjectList = mn.GetListByLevel(Level.level.GetLevelName());


        //var go1 = createGameObject(new ShelfObject("FromBlender/omokutu", "Omo Deterjan", -3, 1.0f, -12));
        //var go2 = createGameObject(new ShelfObject("FromBlender/pinarsut", "Pınar Süt", -3.0f, 2.0f, -12));

        //var go1 = createGameObject("FromBlender/omokutu", "Omo Deterjan", -3, 1.0f, -12);
        //var go2 = createGameObject("FromBlender/pinarsut", "Pınar Süt", -3.0f, 2.0f, -12);

        foreach (var props in Level.level.shelfObjects)
        {
            var newObject = CreateGameObject(props);
            var objectIfSelected = listItems.Where(h => h.go.name == props.name).FirstOrDefault();
            if (props.visibleInList && objectIfSelected == null)
                listItems.Add(new Shoppable(newObject, false));

            SetPassiveShelfObject(props.shelfName);
            SetActiveShelfObject(props.activateShelfName);
        }
    }
    
    public bool CheckItem(GameObject item)
    {
        for (int i = 0; i < listItems.Count; i++)
        {
            if (listItems[i].go.name == item.name && listItems[i].found != true)
            {
                listItems[i].found = true;
                Debug.Log(item.name + " was found in list");
                return true;
            }
        }
        return false;
    }

    public string GetListText()
    {
        string resultText = "\n\n Alışveriş Listesi";
        for (var i = 0; i < this.listItems.Count; i++)
        {
            string checkedStr = this.listItems[i].found ? " + " : "";
            resultText += "\n " + (i + 1) + "- " + checkedStr + this.listItems[i].go.name;
        }
        return resultText;
    }

    public bool IsAllItemsFound()
    {
        if (this.listItems.Count() == this.listItems.Where(h => h.found == true).Count())
            return true;
        return false;
    }

    private GameObject CreateGameObject(ShelfObject shelfObj)
    {
        GameObject obj = Resources.Load<GameObject>(shelfObj.path);
        obj.transform.localScale = new Vector3(shelfObj.sx, shelfObj.sy, shelfObj.sz);
        var pos = new Vector3(shelfObj.px, shelfObj.py, shelfObj.pz);
        GameObject objInstantiated = Instantiate(obj, pos, shelfObj.rotation);
        objInstantiated.name = shelfObj.name;
        objInstantiated.AddComponent<BoxCollider>();
        return objInstantiated;
    }

    private void SetPassiveShelfObject(string name)
    {
        if (!string.IsNullOrEmpty(name))
        {
            if (shelfObject == null)
                shelfObject = GameObject.FindGameObjectWithTag(name);
            else
            {
                if (shelfObject.name != name)
                    shelfObject = GameObject.FindGameObjectWithTag(name);
            }

            if (shelfObject != null)
                shelfObject.SetActive(false);
        }
    }
    private void SetActiveShelfObject(string name)
    {
        if (!string.IsNullOrEmpty(name))
        {
            if (activateShelfObject == null)
                activateShelfObject = GameObject.FindGameObjectWithTag(name);
            else
            {
                if (activateShelfObject.name != name)
                    activateShelfObject = GameObject.FindGameObjectWithTag(name);
            }

            if (activateShelfObject != null)
                activateShelfObject.SetActive(true);
        }
    }

}
